function initialize() {
	var numberOfLocation = 0;
	var bangkokLatLng = new google.maps.LatLng(13.736717, 100.523186);
	var form = $("#busForm");

	var styles = [ {
		"featureType" : "transit",
		"stylers" : [ {
			"visibility" : "off"
		} ]
	} ];
	var styledMap = new google.maps.StyledMapType(styles, {
		name : "Styled Map"
	});

	var mapOptions = {
		center : bangkokLatLng,
		zoom : 8,
		mapTypeControlOptions : {
			mapTypeIds : [ google.maps.MapTypeId.ROADMAP, 'map_style' ]
		}
	};

	var map = new google.maps.Map(document.getElementById('map-canvas'),
			mapOptions);

	var marker = new google.maps.Marker({
		draggable : true,
		position : bangkokLatLng,
		map : map,
		animation : google.maps.Animation.DROP
	});

	google.maps.event.addListener(marker, 'dragend', markerDrop);

	function markerDrop() {
		var lat = marker.getPosition().lat();
		var lng = marker.getPosition().lng();
		$(".latitude").val(lat);
		$(".longitude").val(lng);
	}

	function getBox(latitude, longitude) {
		var input = '<div class="location-box"><input type="text" class="span6 lat" value="'
				+ latitude
				+ '" disabled="true" /><input type="text" class="span6 lng"  value="'
				+ longitude + '" disabled="true" /></div>';
		return input;
	}

	$("#btn-add").click(function() {
		var latitude = $(".latitude").val();
		var longitude = $(".longitude").val();
		if (latitude != "" && longitude != "") {
			addCoordinateItem(latitude, longitude);
			$(".latitude").val("");
			$(".longitude").val("");
		} else {
			alert("latitude and longitude cannot be null.");
		}
	});

	function addCoordinateItem(latitude, longitude) {
		var boxItem = getBox(latitude, longitude);
		$(".location-container").append(boxItem);
		addHidden(form, latitude, longitude);
		itemChange();
	}

	function bindMapResponse(data) {
		$("#name").val(data.wObject.name);
		data.wObject.coordinates.forEach(function(entry) {
			addCoordinateItem(entry.latitude, entry.longitude);
		});
	}

	function addHidden(theForm, latitude, longitude) {
		var inputLat = document.createElement('input');
		inputLat.type = 'hidden';
		inputLat.id = 'coordinates' + numberOfLocation + '.latitude';
		inputLat.name = 'coordinates[' + numberOfLocation + '].latitude';
		inputLat.value = latitude;

		var inputLng = document.createElement('input');
		inputLng.type = 'hidden';
		inputLng.id = 'coordinates' + numberOfLocation + '.longitude';
		inputLng.name = 'coordinates[' + numberOfLocation + '].longitude';
		inputLng.value = longitude;

		theForm.append(inputLat);
		theForm.append(inputLng);
		numberOfLocation++;
	}

	function itemChange() {
		var busPlanCoordinates = [];
		$(".location-box").each(
				function(index) {
					var latitude = $(this).children(".lat").val();
					var longitude = $(this).children(".lng").val();
					busPlanCoordinates.push(new google.maps.LatLng(latitude,
							longitude));
				});

		var busPath = new google.maps.Polyline({
			path : busPlanCoordinates,
			geodesic : true,
			strokeColor : '#E64A19',
			strokeOpacity : 1.0,
			strokeWeight : 2
		});

		busPath.setMap(map);
	}

	$("#submitForm").click(function() {
		submitForm();
		return false;
	})

	function submitForm() {
		var fd = new FormData(document.getElementById("kmlUpload"));
		fd.append("label", "WEBUPLOAD");
		$.ajax({
			url : "/admin/bus/upload",
			type : "POST",
			data : fd,
			enctype : 'multipart/form-data',
			processData : false,
			contentType : false
		}).done(function(data) {
			bindMapResponse(data);
		});
	}

}

google.maps.event.addDomListener(window, 'load', initialize);