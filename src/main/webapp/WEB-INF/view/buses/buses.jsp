<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@taglib uri="http://www.atg.com/taglibs/json" prefix="json"%>

<json:object>

	<json:object name="pageInfomation">
		<json:property name="number" value="${wObject.pageInfomation.number}" />
		<json:property name="size" value="${wObject.pageInfomation.size}" />
	</json:object>
	<json:property name="numberOfEntities" value="${wObject.totalNumberOfEntities}" />

	<json:array name="items" var="item" items="${wObject.entities}">
		<json:object>
			<json:property name="name" value="${item.name}" />
			<json:property name="description" value="${item.description}" />
			<json:array name="item" var="road" items="${item.roads}">
				<json:object>
					<json:property name="latitude" value="${road.latitude}" />
					<json:property name="longitude" value="${road.longitude}" />
				</json:object>
			</json:array>
		</json:object>
	</json:array>

</json:object>