<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@taglib uri="http://www.atg.com/taglibs/json" prefix="json"%>

<json:object>
	<json:property name="name" value="${wObject.name}" />
	<json:property name="description" value="${wObject.description}" />
	<json:array name="item" var="road" items="${wObject.roads}">
		<json:object>
			<json:property name="latitude" value="${road.latitude}" />
			<json:property name="longitude" value="${road.longitude}" />
		</json:object>
	</json:array>
</json:object>