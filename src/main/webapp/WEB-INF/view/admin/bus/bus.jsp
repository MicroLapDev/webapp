<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@taglib tagdir="/WEB-INF/tags/common" prefix="common"%>

<common:default-main title="Admin Main Page" checkUser="true">

	<c:if test="${not empty wObject.entities }">
		<c:set var="_page" value="${wObject.entities}" />
	</c:if>

	<div class="btn-controls">
		<div class="btn-box-row row-fluid">
			<div class="span12">
				<div class="btn-box-row row-fluid">
					<a href="/admin/bus?_f=create" class="btn-box small span2"> <i class="icon-plus-sign"></i> <b>Add</b>
					</a> <a href="/admin/bus?_a=index" class="btn-box small span2"> <i class="icon-plus-sign"></i> <b>ReIndex</b>
					</a>
				</div>
				<%@include file="../common/bus-table.jspf"%>
			</div>
		</div>
	</div>

</common:default-main>