<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@taglib tagdir="/WEB-INF/tags/common" prefix="common"%>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script src="${param.contextPath}/static/js/toiletmap.js" type="text/javascript"></script>
<link type="text/css" href="${param.contextPath}/static/css/toilet.css" rel="stylesheet">
<link type="text/css" href="${param.contextPath}/static/css/add-bus.css" rel="stylesheet">

<common:default-main title="Admin Main Page" checkUser="true">

	<c:if test="${not empty wObject.entities }">
		<c:set var="_page" value="${wObject.entities}" />
	</c:if>

	<div class="btn-controls">
		<div class="btn-box-row row-fluid">
			<div class="span12">
				<%@include file="bus-form.jspf"%>
			</div>
		</div>
	</div>

</common:default-main>