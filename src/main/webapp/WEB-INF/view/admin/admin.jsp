<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@taglib tagdir="/WEB-INF/tags/common" prefix="common"%>

<common:default-main title="Admin Main Page" checkUser="true">

	<div class="btn-controls">
		<div class="btn-box-row row-fluid">
			<a href="#" class="btn-box big span4"> <i class=" icon-random"></i> <b>${wObject.totalNumberOfEntities}</b>
				<p class="text-muted">Total Bus</p>
			</a> <a href="#" class="btn-box big span4"><i class="icon-user"></i><b>0</b>
				<p class="text-muted">Approved</p> </a><a href="#" class="btn-box big span4"><i class="icon-money"></i><b>15,152</b>
				<p class="text-muted">Profit</p> </a>
		</div>
		<div class="btn-box-row row-fluid">
			<div class="span12">
				<%@include file="common/bus-table.jspf"%>
			</div>
		</div>
	</div>

</common:default-main>