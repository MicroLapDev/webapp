<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@taglib tagdir="/WEB-INF/tags/common" prefix="common"%>

<common:main title="Index Web" checkUser="false" showSearch="false">

	<div class="row">
		<div class="module module-login span4 offset4">
			<form class="form-vertical" name='loginForm' action="<c:url value='/login' />" method='POST'>
				<div class="module-head">
					<h3>Sign In</h3>
				</div>
				<div class="module-body">
					<div class="control-group">
						<div class="controls row-fluid">
							<input class="span12" type="text" name='username' id="inputEmail" placeholder="Username">
						</div>
					</div>
					<div class="control-group">
						<div class="controls row-fluid">
							<input class="span12" type="password" name='password' id="inputPassword" placeholder="Password">
						</div>
					</div>
				</div>
				<div class="module-foot">
					<div class="control-group">
						<div class="controls clearfix">
							<button type="submit" class="btn btn-primary pull-right">Login</button>
							<label class="checkbox"> <input type="checkbox"> Remember me
							</label>
						</div>
					</div>
				</div>
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			</form>
		</div>
	</div>

</common:main>
