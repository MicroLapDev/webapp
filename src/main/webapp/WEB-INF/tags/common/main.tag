<%@tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@attribute name="bodyClass" required="false" rtexprvalue="true"%>
<%@attribute name="checkUser" type="java.lang.Boolean" required="true" rtexprvalue="true"%>
<%@attribute name="showSearch" type="java.lang.Boolean" required="false" rtexprvalue="true"%>
<%@attribute name="title" rtexprvalue="true"%>

<c:if test="${empty  showSearch}">
	<c:set var="showSearch" value="true" />
</c:if>
<jsp:doBody var="_content" />

<!DOCTYPE html>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link type="text/css" href="${param.contextPath}/static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link type="text/css" href="${param.contextPath}/static/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
<link type="text/css" href="${param.contextPath}/static/css/theme.css" rel="stylesheet">
<link type="text/css" href="${param.contextPath}/static/css/admin.css" rel="stylesheet">
<link type="text/css" href="${param.contextPath}/static/images/icons/css/font-awesome.css" rel="stylesheet">
<link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
	rel='stylesheet'>


<title>${title}</title>
</head>
<body>
	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse"> <i
					class="icon-reorder shaded"></i></a><a class="brand" href="index.html">Road Bus Admin</a>
				<div class="nav-collapse collapse navbar-inverse-collapse">
					<c:if test="${showSearch }">
						<form class="navbar-search pull-left input-append" action="#">
							<input type="text" class="span3">
							<button class="btn" type="button">
								<i class="icon-search"></i>
							</button>
						</form>
					</c:if>

					<c:if test="${checkUser}">
						<ul class="nav pull-right">
							<c:if test="${not empty user}">
								<li><a>${user.username}</a></li>
								<li><a>${user.aboutMe}</a></li>
							</c:if>
							<li class="nav-user dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"> <img
									src="images/user.png" class="nav-avatar" /> <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><a href="#">Your Profile</a></li>
									<li><a href="#">Edit Profile</a></li>
									<li><a href="#">Account Settings</a></li>
									<li class="divider"></li>
									<li><a href="#" onclick="logoutSubmit()">Logout</a></li>
								</ul></li>
						</ul>
					</c:if>
				</div>
			</div>
		</div>
	</div>

	<div class="wrapper">
		<div class="container">${_content}</div>
	</div>

	<div class="footer">
		<div class="container">
			<b class="copyright">&copy; 2014 Edmin - EGrappler.com </b>All rights reserved.
		</div>
	</div>

	<c:url value="/logout" var="logoutUrl" />
	<form action="${logoutUrl}" method="post" id="logoutForm">
		<input type="hidden" name="${_csrf.parameterName }" value="${_csrf.token }">
	</form>

	<script src="${param.contextPath}/static/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
	<script src="${param.contextPath}/static/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
	<script src="${param.contextPath}/static/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="${param.contextPath}/static/scripts/flot/jquery.flot.js" type="text/javascript"></script>
	<script src="${param.contextPath}/static/scripts/flot/jquery.flot.resize.js" type="text/javascript"></script>
	<script src="${param.contextPath}/static/scripts/datatables/jquery.dataTables.js" type="text/javascript"></script>
	<script src="${param.contextPath}/static/scripts/common.js" type="text/javascript"></script>
	<script src="${param.contextPath}/static/js/toilet.js" type="text/javascript"></script>
</body>
</html>