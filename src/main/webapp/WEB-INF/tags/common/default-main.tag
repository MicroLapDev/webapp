<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@taglib tagdir="/WEB-INF/tags/common" prefix="common"%>
<%@attribute name="bodyClass" required="false" rtexprvalue="true"%>
<%@attribute name="checkUser" type="java.lang.Boolean" required="true" rtexprvalue="true"%>
<%@attribute name="showSearch" type="java.lang.Boolean" required="false" rtexprvalue="true"%>
<%@attribute name="title" rtexprvalue="true"%>

<jsp:doBody var="_content" />

<common:main title="${title}" checkUser="${checkUser}" showSearch="${showSearch}" bodyClass="${bodyClass}">
	<div class="row">
		<div class="span3">
			<div class="sidebar">

				<ul class="widget widget-menu unstyled">
					<li class="active"><a href="/admin"><i class="menu-icon icon-dashboard"></i>Dashboard </a></li>
					<li><a href="activity.html"><i class="menu-icon icon-bullhorn"></i>News Feed </a></li>
				</ul>

				<ul class="widget widget-menu unstyled">
					<li><a href="/admin/bus"><i class="menu-icon icon-bold"></i>Bus</a></li>
					<li><a href="ui-button-icon.html"><i class="menu-icon icon-bold"></i>Approved</a></li>
				</ul>

				<ul class="widget widget-menu unstyled">
					<li><a class="collapsed" data-toggle="collapse" href="#togglePages"><i class="menu-icon icon-cog"> </i><i
							class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right"> </i>Management</a>
						<ul id="togglePages" class="collapse unstyled">
							<li><a href="/admin/users"><i class="icon-inbox"></i> User</a></li>
							<li><a href="other-user-profile.html"><i class="icon-inbox"></i> Profile </a></li>
						</ul></li>
					<li><a href="#" onclick="logoutSubmit()"><i class="menu-icon icon-signout"></i>Logout</a></li>
				</ul>

			</div>

		</div>

		<div class="span9">
			<div class="content">${_content}</div>
		</div>

	</div>
</common:main>