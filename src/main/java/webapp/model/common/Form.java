package webapp.model.common;

public interface Form<R> {

	/**
	 * Initialize form.
	 */
	public void initialize();

	/**
	 * @return result
	 */
	public R process();

	/**
	 * 
	 * @return entity
	 */
	public R createEntity();
}
