package webapp.model.common;

import java.io.Serializable;

public class PageInfomation implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int number;
	private int size;

	/**
	 * Gets number.
	 * 
	 * @return number
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * The number to set.
	 * 
	 * @param number
	 *            number
	 */
	public void setNumber(int number) {
		this.number = number;
	}

	/**
	 * Gets size.
	 * 
	 * @return size
	 */
	public int getSize() {
		return size;
	}

	/**
	 * The size to set.
	 * 
	 * @param size
	 *            size
	 */
	public void setSize(int size) {
		this.size = size;
	}

	/**
	 * Create with strong type parameter.
	 * 
	 * @param number
	 *            number
	 * @param size
	 *            size
	 * @return
	 */
	public static PageInfomation create(int number, int size) {
		PageInfomation page = new PageInfomation();
		page.number = number;
		page.size = size;
		return page;
	}
}
