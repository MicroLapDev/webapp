package webapp.model.user;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.security.crypto.password.PasswordEncoder;

import webapp.model.common.Form;
import webapp.web.repository.user.external.UserRepository;
import webapp.web.repository.user.external.UserRoleRepository;

public class UserForm implements Form<User> {
	private UserRepository userRepository;
	private PasswordEncoder passwordEncode;
	private UserRoleRepository userRoleRepository;

	@NotNull
	private String username;
	@NotNull
	@Min(4)
	private String password;
	@NotNull
	@Min(4)
	private String retryPassword;

	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public void setPasswordEncode(PasswordEncoder passwordEncode) {
		this.passwordEncode = passwordEncode;
	}

	public void setUserRoleRepository(UserRoleRepository userRoleRepository) {
		this.userRoleRepository = userRoleRepository;
	}

	@Override
	public void initialize() {

	}

	@Override
	public User process() {
		User user = new User();
		user.setUsername(username);
		user.setPassword(passwordEncode.encode(password));
		user.setEnabled(true);
		userRepository.save(user);

		UserRole userRole = new UserRole();
		userRole.setRole(UserRole.ROLE_ADMIN);
		user.getUserRole().add(userRole);
		userRole.setUser(user);
		userRoleRepository.save(userRole);

		return user;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRetryPassword() {
		return retryPassword;
	}

	public void setRetryPassword(String retryPassword) {
		this.retryPassword = retryPassword;
	}

	@Override
	public User createEntity() {
		return null;
	}

}
