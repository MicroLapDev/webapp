package webapp.model.user;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class LoginUser extends User implements Serializable {

	public LoginUser(String username, String password, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
	}

	public LoginUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String aboutMe;

	/**
	 * Gets about me.
	 * 
	 * @return about me.
	 */
	public String getAboutMe() {
		return aboutMe;
	}

	/**
	 * Set about me.
	 * 
	 * @param aboutMe
	 *            aboutMe
	 */
	public void setAboutMe(String aboutMe) {
		this.aboutMe = aboutMe;
	}

	public static LoginUser create(webapp.model.user.User user, List<GrantedAuthority> authorities) {
		LoginUser lUser = new LoginUser(user.getUsername(), user.getPassword(), user.isEnabled(), true, true, true,
				authorities);
		return lUser;
	}
}
