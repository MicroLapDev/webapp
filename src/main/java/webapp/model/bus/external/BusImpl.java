package webapp.model.bus.external;

import java.io.Serializable;
import java.sql.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import webapp.model.bus.Bus;
import webapp.model.common.BaseModel;

@Entity
@Table(name = "bus")
public class BusImpl extends BaseModel implements Bus, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private int id;

	private String name;
	private String description;

	@Column(name = "start_trip")
	private String startTrip;
	@Column(name = "return_trip")
	private String returnTrip;
	@Column(name = "update_time")
	private Date updateTime;
	@Column(name = "create_time")
	private Date createTime;

	@OneToMany(mappedBy = "bus")
	private Set<RoadImpl> roads;

	/**
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 *            id
	 */
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Set<RoadImpl> getRoads() {
		return roads;
	}

	public void setRoads(Set<RoadImpl> roads) {
		this.roads = roads;
	}

	public String getStartTrip() {
		return startTrip;
	}

	public void setStartTrip(String startTrip) {
		this.startTrip = startTrip;
	}

	public String getReturnTrip() {
		return returnTrip;
	}

	public void setReturnTrip(String returnTrip) {
		this.returnTrip = returnTrip;
	}
}
