package webapp.model.bus.external;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Latitude;
import org.hibernate.search.annotations.Longitude;
import org.hibernate.search.annotations.Spatial;
import org.hibernate.search.spatial.Coordinates;

import webapp.model.bus.Road;
import webapp.model.common.BaseModel;

@Entity
@Spatial
@Indexed
@Table(name = "road")
public class RoadImpl extends BaseModel implements Road, Coordinates {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private int id;
	@Latitude
	private Double latitude;
	@Longitude
	private Double longitude;

	@ManyToOne
	private BusImpl bus;

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setId(int id) {
		this.id = id;
	}

	@Override
	public Double getLatitude() {
		return latitude;
	}

	@Override
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	@Override
	public Double getLongitude() {
		return longitude;
	}

	@Override
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public BusImpl getParentId() {
		return bus;
	}

	public void setParentId(BusImpl bus) {
		this.bus = bus;
	}

}
