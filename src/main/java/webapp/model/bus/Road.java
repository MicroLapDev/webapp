package webapp.model.bus;

public interface Road {

	public int getId();

	public void setId(int id);

	public Double getLatitude();

	public void setLatitude(Double latitude);

	public Double getLongitude();

	public void setLongitude(Double longitude);
}
