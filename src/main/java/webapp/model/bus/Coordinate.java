package webapp.model.bus;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import webapp.model.bus.external.BusImpl;
import webapp.model.bus.external.RoadImpl;

public class Coordinate implements Serializable {
	private final Logger LOGGER = LoggerFactory.getLogger(Coordinate.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Double latitude;
	private Double longitude;

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public RoadImpl create(BusImpl busImpl) {
		RoadImpl roadImpl = new RoadImpl();
		roadImpl.setLatitude(latitude);
		roadImpl.setLongitude(longitude);
		roadImpl.setParentId(busImpl);
		LOGGER.info("latitude : " + latitude + "longitude : " + longitude);
		return roadImpl;
	}

}