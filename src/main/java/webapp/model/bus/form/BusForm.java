package webapp.model.bus.form;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import webapp.model.bus.Coordinate;
import webapp.model.bus.external.BusImpl;
import webapp.model.bus.external.RoadImpl;
import webapp.model.common.Form;
import webapp.web.repository.bus.external.BusRepository;
import webapp.web.repository.bus.external.RoadRepository;

public class BusForm implements Form<BusImpl> {
	private final Logger LOGGER = LoggerFactory.getLogger(BusForm.class);

	private BusRepository busRepository;
	private RoadRepository roadRepository;

	@NotNull
	private String name;
	@NotNull
	private String description;
	private String startTrip;
	private String returnTrip;
	private List<Coordinate> coordinates = new ArrayList<Coordinate>();

	@Override
	public void initialize() {

	}

	public void setBusRepository(BusRepository busRepository) {
		this.busRepository = busRepository;
	}

	public void setRoadRepository(RoadRepository roadRepository) {
		this.roadRepository = roadRepository;
	}

	@Override
	public BusImpl process() {
		BusImpl busImpl = busRepository.save(createEntity());

		List<RoadImpl> roads = new ArrayList<RoadImpl>();
		LOGGER.info("Road number : " + coordinates.size());
		for (Coordinate c : coordinates) {
			RoadImpl roadImpl = c.create(busImpl);
			roadRepository.save(roadImpl);
			roads.add(roadImpl);
		}

		busImpl.setRoads(new HashSet<RoadImpl>(roads));
		return busImpl;
	}

	@Override
	public BusImpl createEntity() {
		BusImpl bus = new BusImpl();
		bus.setName(name);
		bus.setDescription(description);
		bus.setStartTrip(startTrip);
		bus.setReturnTrip(returnTrip);
		return bus;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStartTrip() {
		return startTrip;
	}

	public void setStartTrip(String startTrip) {
		this.startTrip = startTrip;
	}

	public String getReturnTrip() {
		return returnTrip;
	}

	public void setReturnTrip(String returnTrip) {
		this.returnTrip = returnTrip;
	}

	public List<Coordinate> getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(List<Coordinate> coordinates) {
		this.coordinates = coordinates;
	}

}
