package webapp.web.repository.user.external;

import org.springframework.stereotype.Component;

import webapp.framework.repo.external.AbstractRepository;
import webapp.model.user.UserRole;
import webapp.web.repository.user.UserRoleRepositoryHibernate;

@Component
public class UserRoleRepository extends AbstractRepository<UserRole> implements UserRoleRepositoryHibernate {

	@Override
	protected Class<?> getEntityImplClass() {
		return UserRole.class;
	}

}
