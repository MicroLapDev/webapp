package webapp.web.repository.user.external;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import webapp.framework.repo.external.AbstractRepository;
import webapp.model.user.User;
import webapp.web.repository.user.UserRepositoryHibernate;

@Component
public class UserRepository extends AbstractRepository<User> implements UserRepositoryHibernate {

	@Override
	public User findByUserName(String username) {
		Criteria criteria = createCriteria();
		criteria.add(Restrictions.eq("username", username));
		criteria.add(Restrictions.eq("enabled", true));
		try {
			return (User) criteria.list().get(0);
		} catch (IndexOutOfBoundsException e) {
			throw new UsernameNotFoundException("User not found.");
		}
	}

	@Override
	protected Class<?> getEntityImplClass() {
		return User.class;
	}

}
