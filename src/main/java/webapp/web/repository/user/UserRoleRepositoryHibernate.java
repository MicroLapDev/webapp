package webapp.web.repository.user;

import webapp.framework.repo.Repository;
import webapp.model.user.UserRole;

public interface UserRoleRepositoryHibernate extends Repository<UserRole> {

}
