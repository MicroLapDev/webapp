package webapp.web.repository.user;

import webapp.framework.repo.Repository;
import webapp.model.user.User;

public interface UserRepositoryHibernate extends Repository<User> {

	public User findByUserName(String username);

}
