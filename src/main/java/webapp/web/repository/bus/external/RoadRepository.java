package webapp.web.repository.bus.external;

import webapp.framework.repo.Repository;
import webapp.model.bus.external.RoadImpl;

public interface RoadRepository extends Repository<RoadImpl> {

	void reIndex();
}
