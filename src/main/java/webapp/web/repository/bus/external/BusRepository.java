package webapp.web.repository.bus.external;

import webapp.framework.common.Page;
import webapp.framework.repo.Repository;
import webapp.model.bus.external.BusImpl;
import webapp.web.controller.bus.SpacialFilter;

public interface BusRepository extends Repository<BusImpl> {

	void reIndex();

	/**
	 * 
	 * @param latitude
	 *            latitude
	 * @param longitude
	 *            longitude
	 * @return
	 */
	Page<BusImpl> findByLocation(SpacialFilter spacial);
}
