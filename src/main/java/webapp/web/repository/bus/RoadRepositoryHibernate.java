package webapp.web.repository.bus;

import org.springframework.stereotype.Component;

import webapp.framework.repo.external.AbstractRepository;
import webapp.model.bus.external.RoadImpl;
import webapp.web.repository.bus.external.RoadRepository;

@Component
public class RoadRepositoryHibernate extends AbstractRepository<RoadImpl> implements RoadRepository {

	@Override
	protected Class<?> getEntityImplClass() {
		return RoadImpl.class;
	}

	@Override
	public void reIndex() {
		try {
			getFullTextSession().createIndexer().startAndWait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
