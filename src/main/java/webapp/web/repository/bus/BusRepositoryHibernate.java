package webapp.web.repository.bus;

import org.apache.lucene.search.Query;
import org.hibernate.search.FullTextQuery;
import org.hibernate.search.query.dsl.Unit;
import org.springframework.stereotype.Component;

import webapp.framework.common.Page;
import webapp.framework.repo.external.AbstractRepository;
import webapp.model.bus.external.BusImpl;
import webapp.web.controller.bus.SpacialFilter;
import webapp.web.repository.bus.external.BusRepository;

@Component
public class BusRepositoryHibernate extends AbstractRepository<BusImpl> implements BusRepository {

	@Override
	protected Class<?> getEntityImplClass() {
		return BusImpl.class;
	}

	@Override
	public void reIndex() {
		try {
			getFullTextSession().createIndexer().startAndWait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Page<BusImpl> findByLocation(SpacialFilter spacialFilter) {
		Query query = createQueryBuilder().spatial().within(spacialFilter.getDistance(), Unit.KM)
				.ofLatitude(spacialFilter.getLatitude()).andLongitude(spacialFilter.getLongitude()).createQuery();

		FullTextQuery fullQuery = getFullTextSession().createFullTextQuery(query, getEntityImplClass());
		return createPage(fullQuery);
	}
}
