package webapp.web.controller.admin;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import webapp.framework.controller.AbstractController;
import webapp.framework.utils.KmlUtils;
import webapp.model.WebKml;
import webapp.model.bus.form.BusForm;
import webapp.web.repository.bus.external.BusRepository;
import webapp.web.repository.bus.external.RoadRepository;
import de.micromata.opengis.kml.v_2_2_0.Document;
import de.micromata.opengis.kml.v_2_2_0.Feature;
import de.micromata.opengis.kml.v_2_2_0.LineString;
import de.micromata.opengis.kml.v_2_2_0.Placemark;

@Controller
@RequestMapping("/admin")
public class AdminBusController extends AbstractController {
	private final Logger LOGGER = LoggerFactory.getLogger(AdminBusController.class);
	@Autowired
	private BusRepository busRepository;
	@Autowired
	private RoadRepository roadRepository;

	@RequestMapping(value = "/bus", method = RequestMethod.GET)
	public ModelAndView adminBus() {
		ModelAndView model = createModelAndView("admin/bus/bus");
		model.addObject("wObject", busRepository.findAll());
		return model;
	}

	@RequestMapping(value = "/bus", method = RequestMethod.GET, params = "_f=create")
	public ModelAndView createBus() {
		ModelAndView model = createModelAndView("admin/bus/add-bus");
		model.addObject("busForm", new BusForm());
		return model;
	}

	@RequestMapping(value = "/bus", method = RequestMethod.POST)
	public ModelAndView addBus(@Valid @ModelAttribute("busForm") BusForm form, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			ModelAndView modelAndView = createModelAndView("admin/bus/add-bus");
			return modelAndView;
		}

		form.setBusRepository(busRepository);
		form.setRoadRepository(roadRepository);
		LOGGER.info("Form param name : " + form.getName());
		process(form, bindingResult);
		return new ModelAndView("redirect:/admin/bus");
	}

	@RequestMapping(value = "/bus", method = RequestMethod.GET, params = "_a=index")
	public ModelAndView roadsIndex() {
		ModelAndView model = new ModelAndView("admin/bus/bus");
		busRepository.reIndex();
		return model;
	}

	@RequestMapping(value = "/bus/upload", method = RequestMethod.POST)
	public ModelAndView uploadKml(@RequestParam("file") MultipartFile file) {
		ModelAndView modelAndView = new ModelAndView();
		MappingJackson2JsonView view = new MappingJackson2JsonView();
		view.setModelKey("wObject");
		modelAndView.setView(view);

		if (!file.isEmpty()) {
			try {
				Document document = KmlUtils.parser(file.getInputStream());
				List<Feature> places = document.getFeature();
				return modelAndView.addObject("wObject", getCoordinate(places));
			} catch (Exception e) {
				return modelAndView;
			}

		} else {
			return modelAndView;
		}
	}

	private WebKml getCoordinate(List<Feature> places) {
		WebKml webKml = new WebKml();
		for (Feature feature : places) {
			Placemark placemark = (Placemark) feature;
			webKml.setName(placemark.getName());
			if (placemark.getGeometry() instanceof LineString) {
				LineString line = (LineString) placemark.getGeometry();
				webKml.setCoordinates(line.getCoordinates());
				return webKml;
			}
		}
		return null;
	}
}
