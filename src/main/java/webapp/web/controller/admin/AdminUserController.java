package webapp.web.controller.admin;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import webapp.framework.controller.AbstractController;
import webapp.model.user.UserForm;
import webapp.web.repository.user.external.UserRepository;
import webapp.web.repository.user.external.UserRoleRepository;

@Controller
@RequestMapping("/admin")
public class AdminUserController extends AbstractController {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UserRoleRepository userRoleRepository;
	@Autowired
	private PasswordEncoder passwordEncode;

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public ModelAndView adminUser() {
		ModelAndView model = createModelAndView("admin/user/users");
		model.addObject("wObject", userRepository.findAll());
		return model;
	}

	@RequestMapping(value = "/users", method = RequestMethod.GET, params = "_f=signUp")
	public ModelAndView signUpUser() {
		ModelAndView model = createModelAndView("admin/user/add-user");
		model.addObject("userForm", new UserForm());
		return model;
	}

	@RequestMapping(value = "/users", method = RequestMethod.POST)
	public ModelAndView addUser(@Valid @ModelAttribute("userForm") UserForm form, BindingResult bindingResult) {
		form.setUserRepository(userRepository);
		form.setUserRoleRepository(userRoleRepository);
		form.setPasswordEncode(passwordEncode);

		ModelAndView modelAndView = new ModelAndView();
		if (bindingResult.hasErrors()) {
			modelAndView.addObject("error", true);
			modelAndView.setViewName("admin/user/add-user");
			return modelAndView;
		}

		process(form, bindingResult);
		return new ModelAndView("redirect:/admin/users");
	}

}
