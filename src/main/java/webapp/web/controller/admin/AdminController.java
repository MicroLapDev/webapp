package webapp.web.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import webapp.framework.controller.AbstractController;
import webapp.web.repository.bus.external.BusRepository;

@Controller
@RequestMapping("/admin")
public class AdminController extends AbstractController {

	@Autowired
	private BusRepository busRepository;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView mainAdmin() {
		ModelAndView model = createModelAndView("admin/admin");
		model.addObject("wObject", busRepository.findAll());
		return model;
	}

}
