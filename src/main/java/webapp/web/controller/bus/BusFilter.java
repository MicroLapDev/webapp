package webapp.web.controller.bus;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import webapp.framework.common.Filter;
import webapp.model.common.PageInfomation;

public class BusFilter implements Filter {
	private final Logger LOGGER = LoggerFactory.getLogger(BusFilter.class);

	private String name;
	private int number;
	private int size;

	@Override
	public Criteria createCriteria(Criteria criteria) {
		if (StringUtils.isNotEmpty(name)) {
			criteria.add(Restrictions.like("name", name, MatchMode.ANYWHERE));
			LOGGER.info("Bus filter param name :" + name);
		}
		return criteria;
	}

	/**
	 * @return page infomation.
	 */
	public PageInfomation getPageInfomation() {
		return PageInfomation.create(number, size);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

}
