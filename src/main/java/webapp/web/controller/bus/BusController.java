package webapp.web.controller.bus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import webapp.framework.common.Page;
import webapp.framework.controller.AbstractController;
import webapp.model.bus.external.BusImpl;
import webapp.web.repository.bus.external.BusRepository;
import webapp.web.repository.bus.external.RoadRepository;

@Controller()
public class BusController extends AbstractController {
	private final Logger LOGGER = LoggerFactory.getLogger(BusController.class);
	@Autowired
	private BusRepository busRepository;
	@Autowired
	private RoadRepository roadRepository;

	@RequestMapping(value = "/bus/{id}", method = RequestMethod.GET)
	public ModelAndView bus(@PathVariable int id) {
		ModelAndView model = new ModelAndView("bus/bus");
		model.addObject("wObject", busRepository.findById(id));
		return model;
	}

	@RequestMapping(value = "/buses", method = RequestMethod.GET)
	public ModelAndView buses(@ModelAttribute BusFilter busFilter) {
		ModelAndView model = new ModelAndView("buses/buses");
		model.addObject("wObject", busRepository.findByFilter(busFilter.getPageInfomation(), busFilter));
		return model;
	}

	@RequestMapping(value = "/roads", method = RequestMethod.GET)
	public ModelAndView roads(@ModelAttribute SpacialFilter filter) {
		ModelAndView model = new ModelAndView("buses/buses");
		Page<BusImpl> page = busRepository.findByLocation(filter);
		model.addObject("wObject", page);
		return model;
	}

}
