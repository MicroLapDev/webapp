package webapp.web.controller.bus;

import java.io.Serializable;

public class SpacialFilter implements Serializable {
	private static final Double DEFAULT_DESTANCE = 5d; // 5 KM.

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Double latitude;
	private Double longitude;
	private Double distance;

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getDistance() {
		return distance == null ? DEFAULT_DESTANCE : distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}
}
