package webapp.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import webapp.web.repository.bus.external.BusRepository;

@Controller()
public class MainController {
	@Autowired
	private BusRepository busRepository;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView main() {
		ModelAndView model = new ModelAndView("index");
		model.addObject("wObject", busRepository.findById(2));
		return model;
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login() {
		ModelAndView view = new ModelAndView("admin/login");
		return view;
	}

}
