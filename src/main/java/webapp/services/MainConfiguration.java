package webapp.services;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import webapp.web.repository.user.external.UserRepository;

@Configuration
public class MainConfiguration {

	@Bean
	public UserRepository createUserRepository() {
		return new UserRepository();
	}

}
