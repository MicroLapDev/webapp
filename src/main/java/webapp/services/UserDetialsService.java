package webapp.services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import webapp.model.user.LoginUser;
import webapp.model.user.UserRole;
import webapp.web.repository.user.external.UserRepository;

@Service("userDetailsService")
public class UserDetialsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public LoginUser loadUserByUsername(String username) throws UsernameNotFoundException {
		webapp.model.user.User user = userRepository.findByUserName(username);
		if (user == null) {
			throw new UsernameNotFoundException("Not found user : " + username);
		}
		return LoginUser.create(user, buildUserAuthority(user.getUserRole()));
	}

	private List<GrantedAuthority> buildUserAuthority(Set<UserRole> userRoles) {
		Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();
		for (UserRole userRole : userRoles) {
			setAuths.add(new SimpleGrantedAuthority(userRole.getRole()));
		}
		List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>(setAuths);
		return Result;
	}
}
