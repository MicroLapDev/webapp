package webapp.framework.utils;

import org.springframework.validation.BindingResult;

import webapp.model.common.Form;

public class FormUtials {

	private FormUtials() {

	}

	public static <R> R processForm(Form<R> form, BindingResult bindingResult) {
		if (form != null && !bindingResult.hasErrors()) {
			form.initialize();
			return form.process();
		}
		return null;
	}
}
