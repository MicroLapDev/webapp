package webapp.framework.utils;

import java.io.InputStream;

import de.micromata.opengis.kml.v_2_2_0.Document;
import de.micromata.opengis.kml.v_2_2_0.Kml;

public class KmlUtils {
	private static final Kml KML = new Kml();

	@SuppressWarnings("static-access")
	public static Document parser(InputStream inputStream) {
		return (Document) KML.unmarshal(inputStream).getFeature();
	}
}
