package webapp.framework.common;

import org.hibernate.Criteria;

public interface Filter {
	
	/**
	 * Create Query.
	 * 
	 * @return criteria
	 */
	public Criteria createCriteria(Criteria criteria);
	
}
