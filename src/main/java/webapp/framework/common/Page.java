package webapp.framework.common;

import java.io.Serializable;
import java.util.List;

import webapp.model.common.BaseModel;
import webapp.model.common.PageInfomation;

public class Page<T> extends BaseModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private PageInfomation pageInfomation;
	private int first;
	private int last;
	private int totalNumberOfPage;
	private int totalNumberOfEntities;
	private List<T> entities;

	/**
	 * @return pageInfomation
	 */
	public PageInfomation getPageInfomation() {
		return pageInfomation;
	}

	/**
	 * @param pageInfomation
	 *            pageInfomation
	 */
	public void setPageInfomation(PageInfomation pageInfomation) {
		this.pageInfomation = pageInfomation;
	}

	/**
	 * @return first
	 */
	public int getFirst() {
		return first;
	}

	/**
	 * @param first
	 *            first
	 */
	public void setFirst(int first) {
		this.first = first;
	}

	/**
	 * @return last
	 */
	public int getLast() {
		return last;
	}

	/**
	 * @param last
	 *            last
	 */
	public void setLast(int last) {
		this.last = last;
	}

	/**
	 * @return totalNumberOfPage
	 */
	public int getTotalNumberOfPage() {
		return totalNumberOfPage;
	}

	/**
	 * @param totalNumberOfPage
	 *            totalNumberOfPage
	 */
	public void setTotalNumberOfPage(int totalNumberOfPage) {
		this.totalNumberOfPage = totalNumberOfPage;
	}

	/**
	 * @return totalNumberOfEntities
	 */
	public int getTotalNumberOfEntities() {
		return totalNumberOfEntities;
	}

	/**
	 * @param totalNumberOfEntities
	 *            totalNumberOfEntities
	 */
	public void setTotalNumberOfEntities(int totalNumberOfEntities) {
		this.totalNumberOfEntities = totalNumberOfEntities;
	}

	/**
	 * @return entities
	 */
	public List<T> getEntities() {
		return entities;
	}

	/**
	 * @param entities
	 *            entities
	 */
	public void setEntities(List<T> entities) {
		this.entities = entities;
	}

}
