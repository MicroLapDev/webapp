package webapp.framework.repo;

import org.hibernate.Criteria;
import org.hibernate.Session;

import webapp.framework.common.Filter;
import webapp.framework.common.Page;
import webapp.model.common.PageInfomation;

public interface Repository<T> {

	/**
	 * Gets current session.
	 * 
	 * @return current session
	 */
	public Session getCurrentSession();

	/**
	 * @return criteria
	 */
	public Criteria createCriteria();

	/**
	 * Find entity by id.
	 * 
	 * @param id
	 *            id
	 * @return item
	 */
	public T findById(Integer id);

	/**
	 * 
	 * @return
	 */
	public Page<T> findAll();

	/**
	 * 
	 * @return
	 */
	public Page<T> findByFilter(PageInfomation pageInfomation, Filter filter);

	/**
	 * 
	 * @param item
	 */
	public T save(T item);
}
