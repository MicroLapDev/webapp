package webapp.framework.repo.external;

import javax.persistence.EntityManagerFactory;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.search.FullTextQuery;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import webapp.framework.common.Filter;
import webapp.framework.common.Page;
import webapp.framework.repo.Repository;
import webapp.model.common.BaseModel;
import webapp.model.common.PageInfomation;

@Component
public abstract class AbstractRepository<T extends BaseModel> implements Repository<T> {
	private final Logger LOGGER = LoggerFactory.getLogger(getEntityImplClass());
	private SessionFactory sessionFactory;
	private FullTextSession fullTextSession;

	@Autowired
	public void SomeService(EntityManagerFactory factory) {
		if (factory.unwrap(SessionFactory.class) == null) {
			throw new NullPointerException("factory is not a hibernate factory");
		}
		this.sessionFactory = factory.unwrap(SessionFactory.class);
	}

	@Override
	public Session getCurrentSession() {
		try {
			return sessionFactory.getCurrentSession();
		} catch (Exception e) {
			return sessionFactory.openSession();
		}
	}

	@Override
	public Criteria createCriteria() {
		return getCurrentSession().createCriteria(getEntityImplClass());
	}

	@SuppressWarnings("unchecked")
	@Override
	public T findById(Integer id) {
		return (T) getCurrentSession().get(getEntityImplClass(), id);
	}

	@Override
	public Page<T> findAll() {
		return createPageResult();
	}

	@Override
	public Page<T> findByFilter(PageInfomation pageInfomation, Filter filter) {
		return createPageResult(pageInfomation, filter);
	}

	@SuppressWarnings("unchecked")
	private Page<T> createPageResult() {
		Criteria criteria = createCriteria();

		Page<T> page = new Page<T>();
		page.setEntities(criteria.list());
		page.setTotalNumberOfEntities(getTotalNumberOfEntities());

		return page;
	}

	@SuppressWarnings("unchecked")
	private Page<T> createPageResult(PageInfomation pageInfomation, Filter filter) {
		Criteria criteria = filter.createCriteria(createCriteria());
		criteria.setFirstResult(pageInfomation.getNumber() - 1);
		criteria.setMaxResults(pageInfomation.getSize());

		Page<T> page = new Page<T>();
		page.setEntities(criteria.list());
		page.setPageInfomation(pageInfomation);
		page.setTotalNumberOfEntities(getTotalNumberOfEntities());

		return page;
	}

	private int getTotalNumberOfEntities() {
		return ((Long) createCriteria().setProjection(Projections.rowCount()).uniqueResult()).intValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public T save(T item) {
		Session session = getCurrentSession();
		Integer id = (Integer) session.save(item);
		session.flush();
		LOGGER.info("Finish save item.");
		return (T) session.get(getEntityImplClass(), id);
	}

	public QueryBuilder createQueryBuilder() {
		return getFullTextSession().getSearchFactory().buildQueryBuilder().forEntity(getEntityImplClass()).get();
	}

	public FullTextSession getFullTextSession() {
		if (fullTextSession == null) {
			fullTextSession = Search.getFullTextSession(getCurrentSession());
		}
		return fullTextSession;
	}

	protected Page<T> createPage(FullTextQuery query) {
		Page<T> page = new Page<T>();
		page.setTotalNumberOfEntities(query.getResultSize());
		page.setEntities(query.list());
		return page;
	}

	/**
	 * Gets implementation class of entity defined in Hibernate mapping. This
	 * method must be implemented by subclass.
	 *
	 * @return implementation class of entity
	 */
	protected abstract Class<?> getEntityImplClass();
}
