package webapp.framework.controller;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

import webapp.framework.utils.FormUtials;
import webapp.model.common.Form;
import webapp.model.user.LoginUser;

public abstract class AbstractController {
	/**
	 * 
	 * @param view
	 * @return
	 */
	protected ModelAndView createModelAndView(String view) {
		ModelAndView modelAndView = new ModelAndView(view);
		modelAndView.addObject("user", getLoginUser());
		return modelAndView;
	}

	/**
	 * @return login user.
	 */
	protected LoginUser getLoginUser() {
		return (LoginUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

	/**
	 * Process form.
	 * 
	 * @param form
	 *            form
	 * @param bindingResult
	 *            bindingResult
	 * @return
	 */
	protected <R> R process(Form<R> form, BindingResult bindingResult) {
		return FormUtials.processForm(form, bindingResult);
	}
}
